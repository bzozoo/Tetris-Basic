# Tetris-Basic
It is a simple JavaScript Tetris, what I made based on Ania Kubow's Youitube video, what is viewable [here](https://youtu.be/w1JJfK09ujQ)

### DEMOS
[GITHUB](https://bzozoo.github.io/Tetris-Basic/)

[GITLAB](https://bzozoo.gitlab.io/Tetris-Basic/)

[HEROKU](https://tetrisbasic.herokuapp.com/) 

[VERCEL](https://tetris-basic.vercel.app/) 

[NETLIFY](https://bzozootetris.netlify.app/)

[REPL](https://tetris-basic.zoltnbata.repl.co)

[STACK-BLITZ](https://web-platform-kqfx5n.stackblitz.io)

[CODESANDBOX](https://kul2t.csb.app/)

[JSFIDDLE](https://jsfiddle.net/bzozoo/k6peh230/show)

[CODEPEN](https://codepen.io/bzozoo/full/mdwrwJb)

[ONSERVER](https://tetris.ga/game/)

### REPOS
[GITHUB](https://github.com/bzozoo/Tetris-Basic)

[GITLAB](https://gitlab.com/bzozoo/Tetris-Basic)

<img src="https://raw.githubusercontent.com/bzozoo/Tetris-Basic/master/images/screenshot.png" />

### Planned features
- User system for Tetris
- List of higher scores
- Adjustable speed levels
- Automatical speed increase based on score

### MIT Licence

Copyright (c) 2020 Ania Kubow 2022 Bzozoo

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*Translation: Ofcourse you can use this for you project! Just make sure to say where you got this from :)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
