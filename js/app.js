(function () {
   const application = { name: "TETRIS BASIC", version: "v58" };

   const gameSettings = {
      host: "https://tetris.ga",
      rerenderMode: "onelinebased", // usage: [onelinebased, squarebased, old]
      rerenderable: true,
      width: 10,
      gridNumbers: 250,
      lineNumbers: () => {
         return gameSettings.gridNumbers / gameSettings.width;
      },
      controllButtonsDeactivable: true,
      tetrominoDropable: true,
      displayWidth: 6,
      displayIndex: 1,
      gameSpeedMs: 500,
      levelChange: 300, // Default 300
      scoreStep: 10,
      speedAccelerationFactor: 50,
      boomm: "https://bzozoo.github.io/Tetris-Basic//images/boomm.gif",
      rewardExtraPics: [
         "https://i.imgur.com/awaQZST.jpg",
         "https://i.imgur.com/psZNoKt.jpg",
         "https://i.imgur.com/uCvHYUX.jpg",
         "https://i.imgur.com/rW0gv4H.jpg",
         "https://i.imgur.com/TpZDy0N.jpg",
         "https://i.imgur.com/2EjNoQG.jpg",
         "https://i.imgur.com/KBY1sRA.jpg",
         "https://i.imgur.com/h8SWCow.jpg",
         "https://i.imgur.com/UkUQbSO.jpg",
         "https://i.imgur.com/YEcYcxl.jpg",
         "https://i.imgur.com/voXqUm3.jpg",
         "https://i.imgur.com/cTMV0N2.jpg",
         "https://i.imgur.com/QQfe4tR.jpg",
         "https://i.imgur.com/eaIsfFZ.jpg",
         "https://i.imgur.com/whs4Oze.jpg",
         "https://i.imgur.com/5EvLQKA.jpg",
         "https://i.imgur.com/XMr4syr.jpg",
         "https://i.imgur.com/CuC6EMI.jpg",
         "https://i.imgur.com/uw8zEr8.jpg",
         "https://i.imgur.com/eUgVtPe.jpg"
      ]
   };

   main(gameSettings);

   function main(gameSettings) {
      const {
         host,
         rerenderMode,
         controllButtonsDeactivable,
         gridNumbers,
         width,
         displayWidth,
         displayIndex,
         lineNumbers,
         rewardExtraPics,
         gameSpeedMs,
         boomm
      } = gameSettings;
      console.log("RENDER MODE ::: " + rerenderMode);

      const gameTimer = new IntervalEncapsulator(gameRhythm, gameSpeedMs);
      const speeder = new IntervalEncapsulator(gameRhythm, 100);
      const musicInterval = new IntervalEncapsulator(playMusic, 1);
      let underEffect = false;

      const currents = {
         random: null,
         nextRandom: 0,
         currentPosition: 4,
         currentRotation: 0,
         nextRotation: this.currentRotation + 1,
         current: null,
         nextRotatedCurrent: null
      };

      let {
         random,
         nextRandom,
         currentPosition,
         currentRotation,
         nextRotation,
         current,
         nextRotatedCurrent
      } = currents;
      let score = 0;
      let speed = 1;

      //The Tetrominoes
      /* lTetronimo is the example *
                 0 1 2    0 1 2    0 1 2    0 1 2
                |--------------------------------    
            0   |  X X               X          
        width   |  X      X X X      X      X
    2 * width   |  X          X    X X      X X X        
    */
      const lTetromino = [
         [1, width + 1, width * 2 + 1, 2],
         [width, width + 1, width + 2, width * 2 + 2],
         [1, width + 1, width * 2 + 1, width * 2],
         [width, width * 2, width * 2 + 1, width * 2 + 2]
      ];

      const zTetromino = [
         [0, width, width + 1, width * 2 + 1],
         [width + 1, width + 2, width * 2, width * 2 + 1],
         [0, width, width + 1, width * 2 + 1],
         [width + 1, width + 2, width * 2, width * 2 + 1]
      ];

      const tTetromino = [
         [1, width, width + 1, width + 2],
         [1, width + 1, width + 2, width * 2 + 1],
         [width, width + 1, width + 2, width * 2 + 1],
         [1, width, width + 1, width * 2 + 1]
      ];

      //The Tetrominoes
      /* oTetronimo is the example *
                 0 1 2    0 1 2    0 1 2    0 1 2
                |--------------------------------    
            0   |X X      X X      X X      X X         
        width   |X X      X X      X X      X X
    2 * width   |       
    */

      const oTetromino = [
         [0, 1, width, width + 1],
         [0, 1, width, width + 1],
         [0, 1, width, width + 1],
         [0, 1, width, width + 1]
      ];

      const iTetromino = [
         [1, width + 1, width * 2 + 1, width * 3 + 1],
         [width - 1, width, width + 1, width + 2],
         [1, width + 1, width * 2 + 1, width * 3 + 1],
         [width - 1, width, width + 1, width + 2]
      ];

      const liTetromino = [
         [0, 1, width + 1, width * 2 + 1],
         [2, width, width + 1, width + 2],
         [0, width, width * 2, width * 2 + 1],
         [0, 1, 2, width]
      ];

      const ziTetromino = [
         [1, width, width + 1, width * 2],
         [0, 1, width + 1, width + 2],
         [1, width, width + 1, width * 2],
         [0, 1, width + 1, width + 2]
      ];

      //Tetromino pair with colors arrays
      const theTetrominoes = [
         lTetromino,
         zTetromino,
         tTetromino,
         oTetromino,
         iTetromino,
         liTetromino,
         ziTetromino
      ];
      const tetrominoNames = ["L", "Z", "T", "O", "I", "L-INVERT", "Z-INVERT"];
      const colors = [
         "orange",
         "red",
         "purple",
         "green",
         "blue",
         "brown",
         "turquoise"
      ];

      //the Tetrominos without rotations
      const upNextTetrominoes = [
         [
            2 * displayWidth + 1,
            2 * displayWidth + 2,
            3 * displayWidth + 1,
            4 * displayWidth + 1
         ], //lTetromino
         [
            2 * displayWidth + 1,
            3 * displayWidth + 1,
            3 * displayWidth + 2,
            4 * displayWidth + 2
         ], //zTetromino
         [
            2 * displayWidth + 1,
            3 * displayWidth,
            3 * displayWidth + 1,
            3 * displayWidth + 2
         ], //tTetromino
         [
            2 * displayWidth + 1,
            2 * displayWidth + 2,
            3 * displayWidth + 1,
            3 * displayWidth + 2
         ], //oTetromino
         [
            1 * displayWidth + 1,
            2 * displayWidth + 1,
            3 * displayWidth + 1,
            4 * displayWidth + 1
         ], //iTetromino
         [
            2 * displayWidth + 1,
            2 * displayWidth + 2,
            3 * displayWidth + 2,
            4 * displayWidth + 2
         ], // liTetromino
         [
            2 * displayWidth + 2,
            3 * displayWidth + 1,
            3 * displayWidth + 2,
            4 * displayWidth + 1
         ] // ziTetromino
      ];

      //InitGame
      initGame();
      initRender();
      ////

      ///InitDevFunctions
      //numberisedSquares();
      //numberisedOneline();
      //makeTestLines()
      //arrToArg([230, 238])
      //checkCurrent()
      ////

      function initGame() {
         score = 0;
         gameTimer.stop();
         gameTimer.setTime(gameSpeedMs);
         speed = calcSpeed(score);
         currentPosition = 4;
         currentRotation = 0;
         random = Math.floor(Math.random() * theTetrominoes.length);
         nextRandom = Math.floor(Math.random() * theTetrominoes.length);
         current = theTetrominoes[random][currentRotation];
         nextRotatedCurrent = theTetrominoes[random][nextRotation];
         deactivateGameButtons();
      }

      function initRender() {
         version.innerText = application.version;
         gameOverDiv.classList.add("hideByOpacity");
         highscorecontent.innerHTML = LoadingComponent();
         tryLoginRender();
         getDOMElements().grid.innerHTML = TemplateForGrid(); //Init Grid template
         renderScrore();
         miniSquaresInit();
         rewardPicDivSetHeight(0);
         rewardPicDivSetBackground();
         renderSpeed();
         getDOMElements().startBtn.innerHTML = "▶ START";
         getDOMElements().rewardCheckBox.checked = checkRewardStatus();
         weird.checked = checkWeirdModeInStorage();
      }

      function rewardPicDivSetBackground() {
         const randomReward = Math.floor(
            Math.random() * rewardExtraPics.length
         );
         rewardPicDiv.style.backgroundImage =
            "url(" + rewardExtraPics[randomReward] + ")";
      }

      function miniSquaresInit() {
         const { miniSquares } = getDOMElements();
         miniSquares.forEach((miniSquare, sqkey) => {
            miniSquare.style = "";
            miniSquare.classList.remove("tetromino");
         });
      }

      function draw() {
         const { squares } = getDOMElements();
         current.forEach((index) => {
            squares[currentPosition + index].classList.add("tetromino");
            squares[currentPosition + index].style.backgroundColor =
               colors[random];
         });
      }

      function undraw() {
         const { squares } = getDOMElements();
         current.forEach((index) => {
            squares[currentPosition + index].classList.remove("tetromino");
            squares[currentPosition + index].style.backgroundColor = "";
         });
      }

      function moveLeft() {
         undraw();
         const { squares } = getDOMElements();
         const isAtLeftEdge = current.some(
            (index) => (currentPosition + index) % width === 0
         );
         if (!isAtLeftEdge) currentPosition -= 1;
         if (
            current.some((index) =>
               squares[currentPosition + index].classList.contains("taken")
            )
         ) {
            currentPosition += 1;
         }
         draw();
      }

      function moveRight() {
         undraw();
         const { squares } = getDOMElements();
         const isAtRightEdge = current.some(
            (index) => (currentPosition + index) % width === width - 1
         );
         if (!isAtRightEdge) currentPosition += 1;
         if (
            current.some((index) =>
               squares[currentPosition + index].classList.contains("taken")
            )
         ) {
            currentPosition -= 1;
         }
         draw();
      }

      function moveDown() {
         undraw();
         currentPosition += width;
         draw();
      }

      function gameRhythm() {
         lockCheck() === true && doThisIfLockable();
         lockCheck() === false && moveDown();
      }

      function lockCheck() {
         return checkCurrentInSomeInNextRow() ? true : false;
      }

      function checkCurrentInSomeInNextRow() {
         const { squares } = getDOMElements();
         return current.some((index) =>
            squares[currentPosition + index + width].classList.contains("taken")
         );
      }

      //Lock current tetromino
      function addCurrentToTaken() {
         const { squares } = getDOMElements();
         current.forEach((index) =>
            squares[currentPosition + index].classList.add("taken")
         );
      }

      async function doThisIfLockable() {
         //console.log("DO This if tetromino lockable...")
         addCurrentToTaken(); //Lock current
         checkGameOver() && doItAtGameOver();
         let rowsInTaken = checkRowsInTaken();
         rowsInTaken.length && (await doThisIfRowInTaken(rowsInTaken));
         !checkGameOver() && dropNewTetromino();
      }

      async function doThisIfRowInTaken(rowsInTaken) {
         fasterDownStop();
         tryEffect(rowsInTaken);
         doItAtScoreChangeEvent(rowsInTaken.length);
         await breakBeforeRender(1000);
         setRewardPic();
         doRender(rowsInTaken);
         speed = calcSpeed(score);
         renderSpeed();
         const newSpeedInMs = calcSpeedInMs();
         console.log("NEW SPEED IN MS", newSpeedInMs);
         gameTimer.setTime(newSpeedInMs);
      }

      function doItAtScoreChangeEvent(newScores) {
         addScore(newScores);
         trySaveTheScore(score);
         renderScrore();
      }

      function addScore(lines) {
         score += lines * gameSettings.scoreStep;
      }

      function renderScrore() {
         scoreDisplay.innerHTML = score;
      }

      async function trySaveTheScore(score) {
         const isUserLoggedIn = checkUserIsLoggedIn();
         const newScoreIsBigger = parseInt(localStorage.maxscore) < score;
         const renderableTheSave =
            isUserLoggedIn && newScoreIsBigger ? doSaveScore() : false;
         console.log("Renderable the Save? ", renderableTheSave);
         if (renderableTheSave) {
            eventAtScoreSavedToServer();
            CreateUserMaxScore(score);
            console.log("Stop save score : ", score);
         }
      }

      async function doSaveScore() {
         console.log("Start save score : ", score);
         const response = await sendDataUpdate("score");
         const data = response.status === 200 ? response.json() : false;
         const savesatus = data ? savesatus.isScoreUptate : "false";
         return savesatus;
      }

      function renderSpeed() {
         speedDisplay.innerHTML = speed;
      }

      //Game Over functions
      function checkGameOver() {
         const { squares } = getDOMElements();
         return (
            squares[34].classList.contains("taken") ||
            squares[35].classList.contains("taken") ||
            squares[36].classList.contains("taken")
         );
      }

      function doItAtGameOver() {
         actionsAtGameOver();
         renderAtGameOver();
      }

      function stopTimers() {
         gameTimer.stop();
         speeder.stop();
      }

      function actionsAtGameOver() {
         stopTimers();
         deactivateGameButtons();
      }

      function renderAtGameOver() {
         overScore.innerHTML = "SCORE : " + score;
         gameOverDiv.classList.remove("hideByOpacity");
         console.log("GAME OVERED");
      }

      ///

      function tryEffect(rowsInTaken) {
         console.log("TRYING EFFECT....");
         !underEffect && doEffect(rowsInTaken);
      }

      function doEffect(rowsInTaken) {
         console.log("Do EFFECT ::: ");
         let colorizeThis =
            rerenderMode === "squarebased"
               ? rowsInTaken.map((rowInTaken) => [
                    squareToOnelineNum(rowInTaken[1])
                 ])
               : rowsInTaken;
         colorizeThis.map((rowInTaken) =>
            getOneline(rowInTaken).map(
               (cell) => (cell.style.backgroundImage = `url("${boomm}")`)
            )
         );
         return true;
      }

      function setRewardPic() {
         const calculatedHeigth =
            score !== 0 && score % gameSettings.levelChange === 0
               ? 100
               : (score % gameSettings.levelChange) / 3;
         score !== 0 &&
            score % gameSettings.levelChange === 10 &&
            rewardPicDivSetBackground();
         rewardPicDivSetHeight(calculatedHeigth);
      }

      function rewardPicDivSetHeight(value) {
         rewardPicDiv.style.height = value + "%";
      }

      function breakBeforeRender(time) {
         console.log("BREAK TIME...");
         gameTimer.stop();
         underEffect = true;

         return new Promise((resolve) => {
            setTimeout(() => {
               gameTimer.start();
               underEffect = false;
               resolve(true);
            }, time);
         });
      }

      function doRender(rowsInTaken) {
         console.log("DO RENDER");
         if (gameSettings.rerenderable) {
            switch (rerenderMode) {
               case "onelinebased":
                  //OnelineBasedRerender
                  rerenderOnelines(rowsInTaken);
                  return true;
                  break;
               case "squarebased":
                  //SquareBasedRerender
                  rerenderSquares(rowsInTaken);
                  return true;
                  break;
               case "old":
                  //OldRerender
                  rerenderGrids();
                  return true;
                  break;
               default:
                  return false;
            }
         }

         return false;
      }

      function checkRowsInTaken() {
         switch (rerenderMode) {
            case "onelinebased":
               //OnelineBased checker
               return whichOnelinesInTaken();
               break;
            case "squarebased":
               //SquareBased checker
               return whichSquareInTaken();
               break;
            case "old":
               //Old no checker!
               return false;
               break;
            default:
               return false;
         }
      }

      function dropNewTetromino() {
         if (gameSettings.tetrominoDropable === true) {
            random = nextRandom;
            nextRandom = Math.floor(Math.random() * theTetrominoes.length);
            current = theTetrominoes[random][currentRotation];
            currentPosition = 4;
            draw();
            displayShape();
         }
         console.log("New  tetromino dropped...");
      }

      //OnelineBasedRerender
      function rerenderOnelines(whichOnelinesInTaken) {
         console.log("Oneline based rerender...");
         whichOnelinesInTaken.map((oneline) => {
            removeOnelineFromTaken(oneline);
            !checkWeirdModeInStorage() && jumpOnelineToFirst(oneline);
            removeOnelineFromTetromino(oneline);
         });
         return whichOnelinesInTaken.length;
      }

      function removeOnelineFromTaken(oneline) {
         [
            ...getDOMElements().grid.querySelectorAll(
               `[data-oneline="${oneline}"]`
            )
         ].map((cell) => cell.classList.remove("taken"));
      }

      function removeOnelineFromTetromino(oneline) {
         [
            ...getDOMElements().grid.querySelectorAll(
               `[data-oneline="${oneline}"]`
            )
         ].map((cell) => {
            cell.classList.remove("tetromino");
            cell.style = "";
         });
      }

      function jumpOnelineToFirst(oneline) {
         let firstSquare = getDOMElements().firstSquare();
         getOneline(oneline).map((cell) =>
            firstSquare.insertAdjacentElement("beforebegin", cell)
         );
         //syncSuqares();
      }

      function whichOnelinesInTaken() {
         //Check only in Activegameplace!
         let takenLineList = [];
         for (let oneline = 4; oneline < lineNumbers(); oneline++) {
            let result = checkOnlelineInTaken(oneline);
            result && takenLineList.push(oneline);
         }
         return takenLineList;
      }

      function checkOnlelineInTaken(oneline) {
         return [
            ...getDOMElements().grid.querySelectorAll(
               `[data-oneline="${oneline}"]`
            )
         ].every((cell) => cell.classList.contains("taken"));
      }

      function getOneline(oneline) {
         return [
            ...getDOMElements().grid.querySelectorAll(
               `[data-oneline="${oneline}"]`
            )
         ].map((cell) => cell);
      }

      function getActivegameplaceOneline(oneline) {
         return [
            ...getDOMElements().grid.querySelectorAll(
               `.activegameplace[data-oneline="${oneline}"]`
            )
         ].map((cell) => cell);
      }

      //SquareBasedRerender
      function rerenderSquares(rows) {
         rows.map((row) => {
            uncolorizeRow(row);
            !checkWeirdModeInStorage() && jumpSquaresToFirst(row);
         });
         return rows.length;
      }

      function whichSquareInTaken() {
         const { squares } = getDOMElements();
         let takenLineList = [];
         for (let i = 0; i < squares.length - width - 1; i += width) {
            const row = [
               i,
               i + 1,
               i + 2,
               i + 3,
               i + 4,
               i + 5,
               i + 6,
               i + 7,
               i + 8,
               i + 9
            ];
            if (
               row.every((index) => squares[index].classList.contains("taken"))
            ) {
               takenLineList.push(row);
            }
         }
         return takenLineList;
      }

      function uncolorizeRow(row) {
         const { squares } = getDOMElements();
         row.forEach((index) => {
            squares[index].classList.remove("taken");
            squares[index].classList.remove("tetromino");
            squares[index].style.backgroundColor = "";
         });
      }

      function jumpSquaresToFirst(row) {
         const { squares } = getDOMElements();
         const firstSquare = getDOMElements().firstSquare();
         row.forEach((index) => {
            firstSquare.insertAdjacentElement("beforebegin", squares[index]);
         });
      }

      //OldRerender
      function rerenderGrids() {
         const { squares } = getDOMElements();
         let count = 0;
         for (let i = 0; i < 229; i += width) {
            const row = [
               i,
               i + 1,
               i + 2,
               i + 3,
               i + 4,
               i + 5,
               i + 6,
               i + 7,
               i + 8,
               i + 9
            ];

            if (
               row.every((index) => squares[index].classList.contains("taken"))
            ) {
               console.log("ROW :" + row);

               count += 1;

               //Remove Classes and color from removed SQUARES
               row.forEach((index) => {
                  squares[index].classList.remove("taken");
                  squares[index].classList.remove("tetromino");
                  squares[index].style.backgroundColor = "";
               });

               if (!checkWeirdModeInStorage()) {
                  //Remove Full Squares
                  const squaresRemoved = squares.splice(i, width);

                  //Remove hidden SQUARES from Line0-Line1
                  const hiddenSquaresRemoved = squares.splice(0, width * 3);

                  //Re-add to SQUARES the removed SQUARES
                  squares = squaresRemoved.concat(squares);
                  squares = hiddenSquaresRemoved.concat(squares);

                  //Render new Squares
                  squares.forEach((cell, key) => createNewGridLine(cell, key));
               }
            }
         }
         return count;
      }

      function createNewGridLine(cell, key) {
         //console.log("CELL IN createNewGridLine ::: ")
         //console.log(cell)
         getDOMElements().grid.appendChild(cell);
      }

      ///FIX ROTATION OF TETROMINOS A THE EDGE
      function isAtRight() {
         return current.some(
            (index) => (currentPosition + index + 1) % width === 0
         );
      }

      function isAtLeft() {
         return current.some(
            (index) => (currentPosition + index) % width === 0
         );
      }

      //Rotate functions
      function rotate() {
         undraw();
         if (nextRotationInCollision()) {
            console.log("I CAN NOT TO ROTATE");
         } else {
            currentRotation++;
            if (currentRotation === current.length) {
               //if the current rotation gets to 4, make it go back to 0
               currentRotation = 0;
            }
            current = theTetrominoes[random][currentRotation];
            rotationcorrection();
         }
         draw();
      }

      function rotationcorrection(P) {
         //console.log("CHECK ROTATED POSITION");
         //console.log(P);
         P = P || currentPosition; //get current position.  Then, check if the piece is near the left side.
         if ((P + 1) % width < 4) {
            //add 1 because the position index can be 1 less than where the piece is (with how they are indexed).
            if (isAtRight()) {
               //use actual position to check if it's flipped over to right side
               currentPosition += 1; //if so, add one to wrap it back around
               rotationcorrection(P); //check again.  Pass position from start, since long block might need to move more.
            }
         } else if (P % width > 5) {
            if (isAtLeft()) {
               currentPosition -= 1;
            }
         }
      }

      function nextRotationInCollision() {
         return checkNextRotationInTaken() ? true : false;
      }

      function checkNextRotationInTaken() {
         const { squares } = getDOMElements();
         nextRotation = currentRotation === 3 ? 0 : currentRotation + 1;
         nextRotatedCurrent = theTetrominoes[random][nextRotation];

         return nextRotatedCurrent.some((index) =>
            squares[currentPosition + index].classList.contains("taken")
         )
            ? true
            : false;
      }
      /////////

      //display the shape in the mini-grid display
      function displayShape() {
         getDOMElements().miniSquares.forEach((square) => {
            square.classList.remove("tetromino");
            square.style.backgroundColor = "";
         });
         upNextTetrominoes[nextRandom].forEach((index) => {
            getDOMElements().miniSquares[displayIndex + index].classList.add(
               "tetromino"
            );
            getDOMElements().miniSquares[
               displayIndex + index
            ].style.backgroundColor = colors[nextRandom];
         });
      }

      //TEMPLATES - Template Components
      // Grid template components
      function TemplateForGrid() {
         let gridDivs = "";
         for (let count = 0; gridNumbers > count; count++) {
            let classnames = gridClassCalculator(gridNumbers, count);
            gridDivs += TemplateForGridDiv(
               classnames,
               count,
               Math.ceil((count + 1) / 10)
            );
         }
         return `
         ${gridDivs}   
   `;
      }

      function gridClassCalculator(gridNumbers, count) {
         let classArray = [];
         count <= 29 && classArray.push("hidden");
         gridNumbers - width <= count && classArray.push("taken");
         gridNumbers - width > count &&
            width * 3 <= count &&
            classArray.push("activegameplace");
         divisible(count, width) === count && classArray.push("first-in-row");
         numberLastCharacter(count) === 9 && classArray.push("last-in-row");

         let classnames = classArray.join(" ");
         return classnames;
      }

      function TemplateForGridDiv(classnames, count, oneline) {
         return `
      <div class="${classnames}" data-serialnumber="${count}" data-oneline="${oneline}"></div>
   `;
      }
      // Grid template components END

      //HIGH-SCORE Template Component
      function HighScoreComponent(datas) {
         const highScores = datas
            .map(({ UserName, UserScore }, item) => {
               return `
            <tr><td>${
               item + 1
            }.</td><td>${UserName}</td><td>${UserScore}</td></tr>
       `;
            })
            .join(" ");
         return `
		<div id="highscoreinside">
      <table id="scorelist">
         <tbody>
         <tr>
            <th>Placed</th>
            <th>Name</th>
            <th>Score</th>
         </tr>
         ${highScores}
         </tbody>
      </table>
		</div>	
    `;
      }
      //HIGH-SCORE Template Component END

      //LOADING Component Template
      function LoadingComponent() {
         return `
      <div class="fullcenter">
         <img src="https://bzozoo.github.io/Tetris-Basic//images/loading.gif" alt="loading">
      </div>
   
   `;
      }
      //LOADING Component Template END

      // LOGIN / REGISTER Template components

      function LogRegComponent() {
         return `
      <form id="loginform">
      <fieldset>
      <div id="loginformbox">
         <div class="navbar"><div class="title">LOGIN</div></div>
         <p><label for="username">Username</labe>
         <input id="username" name="username" placeholder="Yourname"/></p>
         <p><label for="passwd">Password</label>
         <input id="passwd" name="passwd" type="password" placeholder="8 character"/></p>
         <div class="fullcenter">
            <button id="loginsend" class="actionBtn">LOGIN</button>
         </div>
      </div>
      </fieldset>
      </form>
      <form id="regform" autocomplete="off">
      <fieldset>
      <div id="regformbox">
         <div class="navbar"><div class="title">REGISTRATION</div></div>
         <p><label for="regusername">Username</labe>
         <input id="regusername" name="regusername" placeholder="Yourname" autocomplete="off" /></p>
         <p><label for="regpasswd">Password (min 8 character)</label>
         <input id="regpasswd" name="regpasswd" type="password" placeholder="8 character" autocomplete="new-password" /></p>
         <p><label for="regpasswdagain">Password-again</label>
         <input id="regpasswdagain" name="regpasswdagain" type="password" placeholder="8 character" /></p>
          <div class="fullcenter">
            <button id="regsend"class="actionBtn">REGISTER</button>
         </div>
      </div>
      </fieldset>
      </form>
   `;
      }

      // LOGIN / REGISTER Template components END

      //USER PROFILE Template Components

      function UserContainerComponent({
         UserName,
         UserScore,
         UserAvatar,
         ExpiredTimeStamp
      }) {
         const avatar =
            UserAvatar === null
               ? '<img src="https://bzozoo.github.io/Tetris-Basic/images/defprofpic.jpg" />'
               : `<img src="${UserAvatar}" />`;
         const ExpiredTime = new Date(ExpiredTimeStamp * 1000).toLocaleString();

         return `
      <div class="navbar">
			<div class="title">Hi, ${UserName}</div>
		</div>
		<fieldset>
				<div id="userpanel">
					<div id="userprofilepicture">
						${avatar}
					</div>
					<div id="userpersonaldatas">
						<p>Your own record</p>
						<h2>${UserScore} point</h2>
					
					
					</div>
				</div>
			<hr />
     		<div class="fullcenter">
         	  <button id="logoutBtn" class="actionBtn">LOGOUT</button>
					<div class="smallfont mt-05rem">Automatical logout at: ${ExpiredTime}</div>
     		</div>
		</fieldset>
   `;
      }

      //USER PROFILE Template Components END

      // TEMPLATES END - Template components END

      //EVENTS

      function fasterDown() {
         speeder.start();
      }

      function fasterDownStop() {
         speeder.stop();
      }

      function extraFastDown() {
         if (!lockCheck()) {
            gameRhythm();
            setTimeout(function () {
               extraFastDown();
            }, 25);
         }
      }

      function control(e) {
         e.keyCode === 37 && moveLeft();
         e.keyCode === 38 && rotate();
         e.keyCode === 39 && moveRight();
         e.keyCode === 35 && extraFastDown();
      }

      function controlDown(e) {
         e.keyCode === 40 && fasterDown();
         document.removeEventListener("keydown", controlDown);
      }

      function controlUp(e) {
         e.keyCode === 40 && fasterDownStop();
         document.addEventListener("keydown", controlDown);
      }

      function activateGameButtons() {
         document.addEventListener("keydown", control);
         document.addEventListener("keydown", controlDown);
         document.addEventListener("keyup", controlUp);
         leftMove.addEventListener("mousedown", moveLeft);
         rightMove.addEventListener("mousedown", moveRight);
         downMove.addEventListener("click", gameRhythm);
         downMove.addEventListener("touchstart", fasterDown);
         downMove.addEventListener("touchend", fasterDownStop);
         downMove.addEventListener("mousedown", fasterDown);
         downMove.addEventListener("mouseup", fasterDownStop);
         rotation.addEventListener("mousedown", rotate);
         resetButton.addEventListener("click", () => {
            initGame();
            initRender();
         });
         console.log("GAME BUTTONS ACTIVATED");
      }

      function deactivateGameButtons() {
         if (controllButtonsDeactivable) {
            document.removeEventListener("keydown", control);
            document.removeEventListener("keydown", controlDown);
            document.removeEventListener("keyup", controlUp);
            controllButtonsSection.innerHTML = controllButtonsSection.innerHTML;
            console.log("GAME BUTTONS DEACTIVATED");
         }
      }

      //scoreBox Open Event
      scoreBox.addEventListener("click", async () => {
         highscorecontent.innerHTML = "";
         highscore.classList.remove("hidden");
         const datas = await getHighScoreSorted();
         const highscorecontentstring =
            datas.error === undefined
               ? await HighScoreComponent(datas)
               : "Server error! Try again later!";
         highscorecontent.innerHTML = highscorecontentstring;
      });

      //scoreBox Close Event
      closeHighScore.addEventListener("click", () => {
         highscore.classList.add("hidden");
      });

      //Login Button Event
      loginBtn.addEventListener("click", () => {
         userzone.classList.remove("hidden");
      });

      //UserZone Close Button Event
      closeUserZone.addEventListener("click", () => {
         userzone.classList.add("hidden");
      });

      // Profile Button Events
      profileBtn.addEventListener("click", () => {
         openUserZone();
         reloadUserZone();
      });

      function openUserZone() {
         userzone.classList.remove("hidden");
      }

      async function reloadUserZone() {
         const userdatas = await getUserDatas();

         if (
            userdatas.UserName !== undefined &&
            userdatas.UserScore !== undefined
         ) {
            usercontainer.innerHTML = UserContainerComponent(userdatas);
            logoutBtn.addEventListener("click", doLogout);
         }

         if (
            userdatas.responsestatus === 401 ||
            (userdatas.UserName !== undefined &&
               userdatas.UserName !== localStorage.UserName)
         ) {
            Swal.fire({
               position: "top-end",
               icon: "error",
               title: "Invalid identifiers! <br />You have been logged out!",
               showConfirmButton: false,
               timer: 3000
            });
            doLogout();
         }

         if (localStorage.maxscore === undefined) {
            Swal.fire({
               position: "top-end",
               icon: "error",
               title:
                  "Maxscore datas was removed! <br />You have been logged out!",
               showConfirmButton: false,
               timer: 5000
            });
            doLogout();
         }

         userdatas.servererror === true &&
            Swal.fire({
               position: "top-end",
               icon: "error",
               title: "Server error!",
               showConfirmButton: false,
               timer: 3000
            });
      }
      // Profile Button Events END

      //START/PAUSE button EVENT
      getDOMElements().startBtn.addEventListener("click", () => {
         let gameTimerStatus = gameTimer.status();
         !gameTimerStatus && activateGameButtons();
         gameTimerStatus && deactivateGameButtons();
         startpauseSwitcher(gameTimer.status());
         startpauseButtonSwitcher(gameTimer.status());
         displayShape();
      });

      function startpauseSwitcher(gameTimerStatus) {
         console.log(" SWITCH START / PAUSE ");
         !gameTimerStatus && gameTimer.start();
         gameTimerStatus && gameTimer.stop();
      }

      function startpauseButtonSwitcher(gameTimerStatus) {
         getDOMElements().startBtn.innerHTML = !gameTimerStatus
            ? "▶ RE-START"
            : "⏸ PAUSE";
      }

      /////

      //Mute button event
      muteButton.addEventListener("click", () => {
         muteSwitcher(musicInterval.status());
         muteButtonSwitcher(musicInterval.status());
      });

      function muteSwitcher(musicIntervalStatus) {
         console.log("MUTE ON / OFF");
         musicIntervalStatus === false && musicInterval.start();
         musicIntervalStatus === true && musicInterval.stop();
         getDOMElements().music.pause();
      }

      function muteButtonSwitcher(musicIntervalStatus) {
         muteButton.innerHTML =
            musicIntervalStatus === false ? "🔇 MUTE" : "🔈 MUTE";
      }

      function playMusic() {
         getDOMElements().music.play();
      }

      ///

      ///Options Button events
      document.addEventListener("keydown", panelcontrol);
      optionsButton.addEventListener("click", toggleOptionPanel);
      closeOPtions.addEventListener("click", toggleOptionPanel);

      function panelcontrol(e) {
         //e.ctrlKey
         e.altKey && e.keyCode === 79 && toggleOptionPanel();
      }

      function toggleOptionPanel() {
         optionspanel.classList.toggle("hidden");
         paneloverlay.classList.toggle("blured");
      }
      ///

      ///Panel Overlay Events
      function closeAllModalsAndOverlay() {
         console.log("CLOSE ALL MODALS");
         paneloverlay.classList.remove("blured");
         [...document.querySelectorAll(".modal")].map((modals) =>
            modals.classList.add("hidden")
         );
      }
      ///

      //Invert chkbox event
      getDOMElements().invertCheckBox.addEventListener(
         "change",
         transformTheGrid
      );

      function transformTheGrid() {
         getDOMElements().grid.style.transform = checkGridInTransform()
            ? ""
            : "rotate(180deg)";
      }

      function checkGridInTransform() {
         return getDOMElements().grid.style.transform === "" ? false : true;
      }
      ///

      //Reward chkbox event
      getDOMElements().rewardCheckBox.addEventListener("change", () => {
         saveRewardStatus();
         console.log("REWARD STATUS:", checkRewardStatus());
         rewardDIVToggler(checkRewardStatus());
      });

      function saveRewardStatus() {
         localStorage.setItem(
            "storedReward",
            getDOMElements().rewardCheckBox.checked
         );
      }

      function rewardDIVToggler(isRewardCheckboxChecked) {
         if (!isRewardCheckboxChecked) {
            rewardPicDiv.classList.add("hideByOpacity");
         } else {
            rewardPicDiv.classList.remove("hideByOpacity");
         }
      }
      ////

      //WeirdMode checkbox event
      weird.addEventListener("change", () => {
         localStorage.setItem("storedWeirdMode", weird.checked);
      });
      ///

      //Login Logout Process Event
      async function loginProcess() {
         console.log("Login process...");
         const loginDatas = await getLoginDatas();

         const everythingIsGood =
            loginDatas.error === undefined &&
            loginDatas.responsestatus === undefined;
         everythingIsGood &&
            doLogin(loginDatas.UserName, loginDatas.UTOK, loginDatas.UserScore);
         loginDatas.responsestatus === 401 &&
            Swal.fire({
               position: "top-end",
               icon: "error",
               title: "Wrong username or password!",
               showConfirmButton: false,
               timer: 3000
            });

         loginDatas.servererror === true &&
            Swal.fire({
               position: "top-end",
               icon: "error",
               title: "Server error!",
               showConfirmButton: false,
               timer: 3000
            });
      }

      function doLogin(username, usertoken, maxscore) {
         CreateAllUserInformations(username, usertoken);
         CreateUserMaxScore(maxscore);
         tryLoginRender();
      }

      function doLogout() {
         const deleteSuccess = deleteAllUserInformations();
         deleteSuccess && emptyPanels();
         deleteSuccess && logoutRender();
      }
      //Login Logout Process Event END

      // Register Events
      async function registerProcess() {
         console.log("Register...");
         regsend.disabled = true;
         const validator = regdatasValidator();

         if (!validator.isValid) {
            !validator.passMatch &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "The two passwords do not match!",
                  showConfirmButton: false,
                  timer: 3000
               });

            !validator.passPower &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "Min 8 characters!",
                  showConfirmButton: false,
                  timer: 3000
               });

            !validator.usernameIsClean &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "Special characters are not allowed in username!",
                  showConfirmButton: false,
                  timer: 3000
               });

            !validator.usernameNotEmpty &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "Username field is empty!",
                  showConfirmButton: false,
                  timer: 3000
               });
         }

         if (validator.isValid) {
            const regdata = await registrationDataSender();
            regdata.responsestatus === 409 &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "User is exist!<br />Choose another name!",
                  showConfirmButton: false,
                  timer: 3000
               });

            regdata.servererror &&
               Swal.fire({
                  position: "top-end",
                  icon: "error",
                  title: "Server Error!",
                  showConfirmButton: false,
                  timer: 3000
               });

            regdata.Registration === "Success" &&
               Swal.fire({
                  position: "top-end",
                  icon: "success",
                  title: "Registration is success!<br /> You can login now ",
                  showConfirmButton: false,
                  timer: 3000
               });
         }
         emptyRegdataFields();
         regsend.disabled = false;
      }
      // Register Events END

      //EVENT RENDERS
      //Login / Logout Event Renders
      function tryLoginRender() {
         emptyPanels();
         const isUserLoggedIn = checkUserIsLoggedIn();
         if (!isUserLoggedIn) {
            document
               .querySelector("#login")
               .insertAdjacentHTML("afterbegin", LogRegComponent());
            loginform.addEventListener("submit", () => {
               event.preventDefault();
               loginProcess();
            });
            regform.addEventListener("submit", () => {
               event.preventDefault(), registerProcess();
            });
         }

         if (isUserLoggedIn) {
            reloadUserZone();
            toggleLoginButtonToProfileButton();
         }
      }

      function logoutRender() {
         toggleLoginButtonToProfileButton();
         document
            .querySelector("#login")
            .insertAdjacentHTML("afterbegin", LogRegComponent());
         loginform.addEventListener("submit", () => {
            event.preventDefault();
            loginProcess();
         });
         regform.addEventListener("submit", () => {
            event.preventDefault(), registerProcess();
         });
      }

      function toggleLoginButtonToProfileButton() {
         document.querySelector("#loginBtn").classList.toggle("hidden");
         document.querySelector("#profileBtn").classList.toggle("hidden");
      }

      //Login /Logout Event Renders END

      //Empty Panels renders
      function emptyPanels() {
         const panels = document.querySelectorAll(".panel");
         [...panels].map((panel) => (panel.innerHTML = ""));
      }
      //Empty Panels renders END

      //Empty regdatas
      function emptyRegdataFields() {
         const regforminput = document.querySelectorAll("#regform input");
         [...regforminput].map((regforminput) => (regforminput.value = ""));
      }
      //Empty regdatas END

      //Event at score saved to server
      function eventAtScoreSavedToServer() {
         scoresavedbox.classList.remove("hidden");
         setTimeout(() => {
            scoresavedbox.classList.add("hidden");
         }, 3000);
      }
      //

      //EVENT RENDERS

      //EVENTS END

      ///FETCH DATA FUNCTIONS
      async function getHighScore() {
         try {
            const response = await fetch(`${host}/api/userscore`);
            const data = (await (response.status === 200))
               ? response.json()
               : {
                    error: `Response staus ::: ${response.status}`,
                    responsestatus: response.status
                 };
            return data;
         } catch (e) {
            return { error: e, servererror: true };
         }
      }

      async function getHighScoreSorted() {
         const data = await getHighScore();
         if (data.error === undefined) {
            let dataByScore = data.slice(0);
            dataByScore.sort(function (a, b) {
               return b.UserScore - a.UserScore;
            });
            return dataByScore;
         }
         return false;
      }

      async function getLoginDatas() {
         const username = loginform.username.value;
         const passwd = loginform.passwd.value;

         const formData = new FormData();
         formData.append("nameField", username);
         formData.append("passField", passwd);

         const options = {
            method: "POST",
            body: formData
         };

         try {
            const response = await fetch(`${host}/api/login`, options);
            const data = (await (response.status === 200))
               ? response.json()
               : {
                    error: `Response staus ::: ${response.status}`,
                    responsestatus: response.status
                 };
            return data;
         } catch (e) {
            return { error: e, servererror: true };
         }
      }

      async function registrationDataSender() {
         const username = regform.regusername.value;
         const passwd = regform.regpasswd.value;

         const formData = new FormData();
         formData.append("reguser", username);
         formData.append("regpwd", passwd);

         const options = {
            method: "POST",
            body: formData
         };

         try {
            const response = await fetch(`${host}/api/register`, options);
            const data = (await (response.status === 201))
               ? response.json()
               : {
                    error: `Response staus ::: ${response.status}`,
                    responsestatus: response.status
                 };
            return data;
         } catch (e) {
            return { error: e, servererror: true };
         }
      }

      async function getUserDatas() {
         const options = {
            method: "POST",
            headers: {
               Authorization: `Bearer ${localStorage.UserJWT}`
            }
         };

         try {
            const response = await fetch(`${host}/api/user`, options);
            const data = (await (response.status === 200))
               ? response.json()
               : {
                    error: `Response staus ::: ${response.status}`,
                    responsestatus: response.status
                 };
            return data;
         } catch (e) {
            return { error: e, servererror: true };
         }
      }

      async function sendDataUpdate(type) {
         let data = "";
         if (type === "score") {
            const grid = document.querySelector(".grid").innerHTML;
            data = `score=${score}&grid=${grid}`;
         }

         const options = {
            method: "POST",
            headers: {
               "Content-Type": "application/x-www-form-urlencoded",
               Authorization: `Bearer ${localStorage.UserJWT}`
            },
            body: data
         };
         const response = await fetch(`${host}/api/user/update`, options);
         const datas = await response.json();
         return datas;
      }
      ///FETCH DATA FUNCTIONS END

      ////HELPERS / Checkers / Calculators
      function getDOMElements() {
         return {
            grid: document.querySelector(".grid"),
            squares: Array.from(document.querySelectorAll(".grid div")),
            firstSquare: function () {
               return this.squares[30];
            },
            squaresInActiveGameplace: Array.from(
               document.querySelectorAll(".activegameplace")
            ),
            firstSquareInRow: document.querySelectorAll(".first-in-row"),
            lastSquareInRow: document.querySelectorAll(".last-in-row"),
            miniSquares: Array.from(
               document.querySelectorAll(".mini-grid div")
            ),
            startBtn: document.querySelector("#start-button"),
            invertCheckBox: document.querySelector("#invert"),
            rewardCheckBox: document.querySelector("#reward"),
            music: document.getElementById("myAudio")
         };
      }

      function divisible(dividend, divisor) {
         if (dividend % divisor == 0) {
            return dividend;
         } else {
            var num = dividend + (divisor - (dividend % divisor));
            return num;
         }
      }

      function numberLastCharacter(number) {
         let string = number.toString();
         return parseInt(string[string.length - 1]);
      }

      function recurs(num, max, recurses = []) {
         if (max >= num) {
            return recurs(num + 1, max, [...recurses, num]);
         }
         return recurses;
      }

      function checkWeirdModeInStorage() {
         return localStorage.getItem("storedWeirdMode") === null
            ? false
            : JSON.parse(localStorage.getItem("storedWeirdMode"));
      }

      function checkRewardStatus() {
         return localStorage.getItem("storedReward") === null
            ? getDOMElements().rewardCheckBox.checked
            : JSON.parse(localStorage.getItem("storedReward"));
      }

      function IntervalEncapsulator(fn, time) {
         let timer = false;
         this.start = function () {
            timer = !this.status() ? setInterval(fn, time) : timer;
         };
         this.stop = function () {
            clearInterval(timer);
            timer = false;
         };
         this.setTime = function (newTime) {
            if (!isNaN(newTime) && newTime != undefined && newTime) {
               console.log("SET NEW TIME ::: " + newTime);
               const oldStatus = this.status();
               this.stop();
               time = newTime;
               oldStatus && this.start();
            }
         };
         this.status = function () {
            return timer !== false;
         };
         this.checkInterval = function () {
            return time;
         };
         this.showDefault = time;
      }

      function Counter({ min, from, step, max, circular = false }) {
         let actual = from;
         const defaults = { min, from, step, max, actual, circular };
         this.incr = function () {
            const newActual = actual + step;
            actual = newActual > max ? (circular ? min : max) : newActual;
            return actual;
         };
         this.decr = function () {
            const newActual = actual - step;
            actual = newActual < min ? (circular ? max : min) : newActual;
            return actual;
         };
         this.reset = function () {
            return ({ min, from, step, max, actual, circular } = defaults);
         };
         this.setStep = function (number) {
            if (typeof number === "number") {
               return (step = number);
            } else {
               return "Not a number";
            }
         };
         this.getActual = function () {
            return actual;
         };
         this.getDefaults = function () {
            return defaults;
         };
      }

      function checkUserIsLoggedIn() {
         return (
            localStorage.hasOwnProperty("UserName") &&
            localStorage.hasOwnProperty("UserJWT") &&
            localStorage.hasOwnProperty("maxscore")
         );
      }

      function CreateAllUserInformations(username, usertoken) {
         localStorage.setItem("UserName", username);
         localStorage.setItem("UserJWT", usertoken);
      }

      function CreateUserMaxScore(score) {
         localStorage.setItem("maxscore", score);
      }

      function deleteAllUserInformations() {
         localStorage.removeItem("UserName");
         localStorage.removeItem("UserJWT");
         localStorage.removeItem("maxscore");
         return true;
      }

      function regdatasValidator() {
         const username = regform.regusername.value;
         const passwd = regform.regpasswd.value;
         const passwdagain = regform.regpasswdagain.value;

         const usernameNotEmpty = username !== "";
         const usernameIsClean =
            username.match(/^[a-zA-Z0-9)\(]+$/g) !== null ? true : false;
         const passPower = passwd.length >= 8;
         const passMatch = passwd === passwdagain;
         const isValid =
            usernameIsClean && passPower && passMatch && usernameNotEmpty;
         return {
            usernameIsClean,
            usernameNotEmpty,
            passPower,
            passMatch,
            isValid
         };
      }

      function calcSpeed(score) {
         return parseInt(
            (score + gameSettings.levelChange) / gameSettings.levelChange
         );
      }

      function calcSpeedInMs() {
         const scoreLevel = calcSpeed(score);
         return (
            gameSpeedMs -
            (scoreLevel - 1) * gameSettings.speedAccelerationFactor
         );
      }

      //HELPERS  / Checkers END

      //DevFunctions
      function numberisedSquares() {
         let counter = 0;
         getDOMElements().squares.map(
            (square) => (square.innerHTML = counter++)
         );
      }

      function numberisedOneline() {
         getDOMElements().squares.map(
            (square) => (square.innerHTML = square.dataset.oneline)
         );
      }

      function squareToOneline(square) {
         const { squares } = getDOMElements();
         return squares[square].dataset.oneline;
      }

      function squareToOnelineNum(square) {
         return parseInt(squareToOneline(square));
      }

      function addOnelineToTaken(oneline) {
         [
            ...getDOMElements().grid.querySelectorAll(
               `[data-oneline="${oneline}"]`
            )
         ].map((cell) => cell.classList.add("taken"));
      }

      function addToTakenToSquares(squaresStart, squaresMax) {
         return recurs(squaresStart, squaresMax).map(
            (item) => (squares[item].className += " taken tetromino")
         );
      }

      function squaresInOneline(oneline) {
         return getOneline(oneline).map((cell) =>
            parseInt(cell.dataset.serialnumber)
         );
      }

      function makeTestLines() {
         addToTakenToSquares(200, 208);
         addToTakenToSquares(210, 218);
         addToTakenToSquares(220, 228);
         addToTakenToSquares(230, 238);
      }

      function arrToArg(squaresArray) {
         return squaresArray.map((squareArray) => squareArray);
      }

      function addOnelineToTakenAndColorizeThat(oneline) {
         addOnelineToTaken(oneline);
         turnToPinkAllTakensOnActiveGameplace();
      }

      function removeAllTakenFromActiveGameplace() {
         const { squaresInActiveGameplace } = getDOMElements();
         squaresInActiveGameplace.map((square) => {
            square.classList.remove("taken");
            turnToPinkAllTakensOnActiveGameplace();
         });
      }

      function removeOneline(oneline) {
         getOneline(oneline).map((cell) => cell.remove());
      }

      function getAllActiveGameplaces() {
         return [...getDOMElements().grid.querySelectorAll(".activegameplace")];
      }

      function turnToPinkActiveGameplace() {
         getAllActiveGameplaces().map(
            (cell) => (cell.style.backgroundColor = "pink")
         );
      }

      function turnToPinkAllTakensOnActiveGameplace() {
         getAllActiveGameplaces().map(
            (cell) =>
               (cell.style.backgroundColor = cell.classList.contains("taken")
                  ? "pink"
                  : "")
         );
      }

      function removeOnelineFromTakenAndUnColorizeThat(oneline) {
         removeOnelineFromTaken(oneline);
         turnToPinkAllTakensOnActiveGameplace();
      }

      function getOnelineNodes(oneline) {
         return getDOMElements().grid.querySelectorAll(
            `.activegameplace[data-oneline="${oneline}"]`
         );
      }

      const getCurrentInSquare = () => [
         parseInt(currentPosition) + parseInt(current[0]),
         parseInt(currentPosition) + parseInt(current[1]),
         parseInt(currentPosition) + parseInt(current[2]),
         parseInt(currentPosition) + parseInt(current[3])
      ];

      const getNextRotatedCurrentInSquare = () => [
         parseInt(currentPosition) + parseInt(nextRotatedCurrent[0]),
         parseInt(currentPosition) + parseInt(nextRotatedCurrent[1]),
         parseInt(currentPosition) + parseInt(nextRotatedCurrent[2]),
         parseInt(currentPosition) + parseInt(nextRotatedCurrent[3])
      ];

      function checkCurrent() {
         const { squares } = getDOMElements();
         console.log(
            "CURRENT TETRROMINO:  " +
               tetrominoNames[random] +
               " - ACTUAL RANDOM: " +
               random +
               " - PROFILE: " +
               current +
               "CURRENT POSITION: " +
               currentPosition
         );

         // Show actual current divlist on Console
         current.forEach((index) => {
            console.log(squares[currentPosition + index]);
         });

         console.log(
            "SQUARES NUMBERS : " + getCurrentInSquare().join(" - ") + " ! "
         );

         nextRotation = currentRotation === 3 ? 0 : currentRotation + 1;
         nextRotatedCurrent = theTetrominoes[random][nextRotation];

         console.log("Next Rotation: " + nextRotation);

         console.log(
            "NEXT ROTATED SQUARES NUMBERS: " +
               getNextRotatedCurrentInSquare().join(" - ") +
               " ! "
         );

         // Show next rotation divlist on Console
         nextRotatedCurrent.forEach((index) => {
            console.log(squares[currentPosition + index]);
         });
      }
   } //MAIN END
})(); //IIFE END
